package name.panitz.game.framework;

import java.util.List;
import java.util.ArrayList;

public abstract class AbstractGame<I,S> implements GameLogic<I,S> {
    /*+ Es folgen zunächst Felder für alle die Objekte, für die es 
      laut Schnittstelle eine Getter-Methode geben soll. */  
  protected List<List<? extends GameObject<I>>> goss = new ArrayList<>();
  protected List<Button> buttons = new ArrayList<>();
  protected List<SoundObject<S>> soundsToPlays= new ArrayList<>();
  protected GameObject<I> player;
  double width;
  double height;

    /*+ Im Konstruktor wird eine Spielfigur und die Größe des sichtbatren Spielfeldes erwartet.*/  
  public AbstractGame(GameObject<I> player, double width, double height) {
    this.player = player;
    this.height = height;
    this.width = width; 
  }
    /*+ Es folgen die Getter-Methoden aus der Schnittstelle, in denen die entsprechenden Felder zurück gegeben werden. */
  @Override public List<List<? extends GameObject<I>>> getGOss(){
    return goss;
  }
  @Override public GameObject<I> getPlayer() {
    return player;
  }
  @Override public List<Button> getButtons(){
    return buttons;
  }
  public List<SoundObject<S>> getSoundsToPlayOnce() {
    return soundsToPlays;
  }
  @Override public double getWidth() {
    return width;
  }
  @Override public double getHeight() {
    return height;
  }

    /*+ Die folgende Methode fügt ein Geräusch in die Liste der als nächstes 
      zu Spielenden Geräusche ein. */
  @Override
  public void playSound(SoundObject<S> so) {
    getSoundsToPlayOnce().add(so);
  }

    /*+ Ein internes Flag wird verwendet, um zu schauen ob das Spiel pausiert.*/    
  private boolean isStopped = false;
  
  @Override public boolean isStopped() {
    return isStopped;
  }
  @Override public void start() {
    isStopped  = false;
  }
  
  @Override public void pause() {
    isStopped  = true;
  }

    /*+ Es gibt keinerlei Reaktion auf Tastatureingaben. */  
  @Override
  public void keyPressedReaction(KeyCode keycode){}
  @Override
  public void keyReleasedReaction(KeyCode keycode){}

}

