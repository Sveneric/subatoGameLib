package name.panitz.game.framework;

public class FallingImage<I> extends ImageObject<I> {
    /*+ Wir verwenden die Svchwerkraft als Konstante. */
  static double G = 9.81;
    /*+ In einem Feld wird die anfängliche Fallgeschwindigkeit vermerkt.*/
  double v0;
  /*+ Und ein Feld notiert, seit wieviel Ticks bereits gesprungen/gefallen wird. */
  int t = 0;
    /* Sind wir am fallen/springen? */  
  public boolean isJumping = false;

    /*+ Als Konstruktor wird der Konstruktor für Bildobjekte verwendet. */  
  public FallingImage(String imageFileName, Vertex corner, Vertex movement) {
    super(imageFileName, corner, movement);
  }
    /*+ Wenn wir den Sprung stoppen wollen, so wird der zuletzt gemachte Weg rückgängig gemacht und die Bewegung auf 0 gesetzt. Wird landen. */
  public void stop() {
    getPos().move(getVelocity().mult(-1.1));
    getVelocity().x = 0;
    getVelocity().y = 0;
    isJumping = false;
  }

  /*+ Wen ngestartet wird....*/
  public void restart() {
    double oldX = getVelocity().x;
    getPos().move(getVelocity().mult(-1.1));
    getVelocity().x = -oldX;
    getVelocity().y = 0;
    isJumping = false;
  }

    /*+ Links/rechts Steuerung geht nur, wenn wir nicht springen. In der Luft lässt sich nicht die Richtung ändern. */
  public void left() {
    if (!isJumping) {
      getVelocity().x = -1;
    }
  }

  public void right() {
    if (!isJumping) {
      getVelocity().x = +1;
    }
  }
    /*+ Wie starten einen Sprung mit der Anfangsgeschwindigkeit. */
  public void jump() {
    if (!isJumping) {
      startJump(-3.5);
    }
  }

  public void startJump(double v0) {
    isJumping = true;
    this.v0 = v0;
    t = 0;
  }
    /*+ Bewegung im Sprung verwendet die Schwerkraft, Anfangsgeschwindigkeit und Falllänge. Das ist Physik.*/
  @Override
  public void move() {
    if (isJumping) {
      t++;
      double v = v0 + G * t / 200;
      getVelocity().y = v;
    }
    super.move();
  }
}
