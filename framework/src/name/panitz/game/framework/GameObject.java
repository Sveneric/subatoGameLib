package name.panitz.game.framework;

/*+ Die Eigenschaften einer Spielfigur werden durch eine Schnittstelle definiert. */
public interface GameObject<I> extends Movable, Paintable<I> {
    /*+ Zunächst gibt es Getter- und Setter-Methoden, wir die Weite und Höhe einer Spielfigur. */
  double getWidth();
  double getHeight();
  void setWidth(double w);
  void setHeight(double h);

    /*+ Es lässt sich die aktuelle Position auf dem Spielfeld erfragen. */  
  Vertex getPos();

    /*+ Der Bewegungsvektor ist zu erfragen und zu setzen. */  
  Vertex getVelocity();
  void setVelocity(Vertex v);

    /*+ Weitere Methoden lassen sich bereits in dieser Schnittstelle las default-Methoden implementieren.

      Ist das Objekt links von einem anderen, ohne es zu berühren? */
  default boolean isLeftOf(GameObject<?> that) {
    return this.getPos().x + this.getWidth() < that.getPos().x;
  }
    /*+ Damit lässt sich auch direkt erfragen, ob  das Objekt rechts von einem anderen ist , ohne es zu berühren? */
  default boolean isRightOf(GameObject<?> that) {
    return that.isLeftOf(this);
  }

    /*+ Was in der Horizontalen geht, ist analog in der Vertikalen zu erfragen.*/
  default boolean isAbove(GameObject<?> that) {
    return this.getPos().y + this.getHeight() < that.getPos().y;
  }

    /*+ Und damit ist leicht definiert, ob sich zwei Objekte berühren. */  
  default boolean touches(GameObject<?> that) {
    if (this.isLeftOf(that)) return false;
    if (that.isLeftOf(this)) return false;
    if (this.isAbove(that))  return false;
    if (that.isAbove(this))  return false;
    return true;
  }

    /*+ Wenn man mit Schwerkraft und Böden arbeitet, ist es oft gut zu wissen,
      ob eine Spielfigur genau auf einer anderen steht. */
  default boolean isStandingOnTopOf(GameObject<?> that) {
    return !(isLeftOf(that) || isRightOf(that)) && isAbove(that)
            && getPos().y + getHeight() + 2 > that.getPos().y;
  }

  
  default void move() {
    getPos().move(getVelocity());
  }
  default double size(){return getWidth() * getHeight();}
  default boolean isLargerThan(GameObject<?> that) {
    return size() > that.size(); 
  }
}

